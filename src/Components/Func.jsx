import { useEffect } from "react";
import { useState } from "react";

const baseUrl = "https://jsonplaceholder.typicode.com/comments";
let pageNum = 0;

function Func() {
  const [comment, setComment] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    getComments(baseUrl, [
      { key: "_page", number: 1 },
      { key: "_limit", number: 10 },
    ]);
  }, []);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  async function getComments(baseUrl, queryParams) {
    setLoading(true)
    const data = await fetch(`${baseUrl}${generateQueryString(queryParams)}`);
    const data_1 = await data.json();
    setComment(data_1);
    setLoading(false)
    return data.headers.get("X-Total-Count");
  }

  const generateQueryString = (queryParams = []) =>
    queryParams.length
      ? `?${queryParams.map((x) => `${x.key}=${x.number}`).join("&")}`
      : "";

  function sortWithEmail() {
    sort("email")
  }
  function sortWithName() {
    sort("name");
  }
  function prevHandler() {
    let count = pageNum < 0 ? --pageNum : pageNum;
    allGet(count);
  }

  function nextHandler() {
    let count = pageNum < 50 ? ++pageNum : pageNum;
    allGet(count);
  }

  function allGet(param) {
    getComments(baseUrl, [
      { key: "_page", number: param },
      { key: "_limit", number: 10 },
    ]);
  }
  function sort(param){
    getComments(baseUrl, [
        { key: "_page", number: pageNum },
        { key: "_limit", number: 10 },
        { key: "_sort", number: param },
      ]);
  }

  return (
    <>
      <div className="pagination">
        <button onClick={prevHandler}>Prev</button>
        <button onClick={nextHandler}>Next</button>
      </div>

      <tbody>
        <tr>
          <th onClick={sortWithEmail}>Email</th>
          <th onClick={sortWithName}>Name</th>
          <th>Comments</th>
        </tr>
        {
            !loading ? 
            comment.length > 0 ? (
                comment.map((x, i) => (
                  <tr key={i}>
                    <td>{x.email}</td>
                    <td>{x.name}</td>
                    <td>{x.body}</td>
                  </tr>
                ))
              ) : (
                <tr>empty</tr>
              )
              :
              <tr>Loading...</tr>
        }
      </tbody>
    </>
  );
}

export default Func;
