import React from "react";

const baseUrl = "https://jsonplaceholder.typicode.com/comments";
let pageNum = 1;

class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: [""],
      loading: false,
      loaded: false,
      paginationNum: pageNum,
    };
  }

  componentDidMount() {
    if (!this.state.loaded && !this.state.loading) {
      this.getComments(baseUrl, [
        { key: "_page", number: pageNum },
        { key: "_limit", number: 10 },
      ]);
    }
  }

  generateQueryString(queryParams = []) {
    return queryParams.length
      ? `?${queryParams.map((x) => `${x.key}=${x.number}`).join("&")}`
      : "";
  }

  async getComments(baseUrl, queryParams) {
    this.setState.loading = true;
    await fetch(`${baseUrl}${this.generateQueryString(queryParams)}`)
      .then((data) => data.json())
      .then((data) => {
        this.setState({
          value: data,
        });
        this.setState.loaded = true;
      });
    this.setState.loading = false;
  }

  prevHandler() {
    let count = pageNum > 0 ? --pageNum : pageNum;
    this.changePages(count)
  }

  nextHandler(val) {
    let count = pageNum < 50 ? ++pageNum : pageNum;
    this.changePages(count)
  }

  sortWithEmail() {
    this.sort("body");
  }

  sortWithName() {
    this.sort("name");
  }
  async changePages(count) {
    this.getComments(baseUrl, [
        { key: "_page", number: count },
        { key: "_limit", number: 10 },
      ]);
  }
  async sort(param) {
    await this.getComments(baseUrl, [
      { key: "_page", number: pageNum },
      { key: "_limit", number: 10 },
      { key: "_sort", number: param },
    ]);
  }

  render() {
    return (
      <>
        <tbody className="pagination">
          <button onClick={() => this.prevHandler()}>Prev</button>
          <button onClick={() => this.nextHandler()}>Next</button>
        </tbody>

        <tbody>
          <tr>
            <th>Email</th>
            <th onClick={() => this.sortWithName()}>Name</th>
            <th onClick={() => this.sortWithEmail()}>Comments</th>
          </tr>
          {!this.state.loading ? (
            this.state.value.length > 0 ? (
              this.state.value.map((item, index) => (
                <tr key={index}>
                  <td>{item.email}</td>
                  <td>{item.name}</td>
                  <td>{item.body}</td>
                </tr>
              ))
            ) : (
              <tr>Empty</tr>
            )
          ) : (
            <tr>Loading...</tr>
          )}
        </tbody>
      </>
    );
  }
}

export default Board;
