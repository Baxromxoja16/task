import "./App.css";
import Func from "./Components/Func";
import Board from "./Components/Class";

function App() {
  return (
    <div className="App">
      <table>
        {/* <Func /> */}
        <Board />
      </table>
    </div>
  );
}

export default App;
